# Omnet++ su MacOS Catalina
Questa guida è basata sulla guida fornita da Omnet++ (https://doc.omnetpp.org/omnetpp/InstallGuide.pdf) adattata da me per far funzionare il tutto su MacOS Catalina (10.15.7)

## Index
- [Requisiti](## Requisiti)
- [Installazione di GDB](## Installazione di GDB) (opzionale)
- [Installazione](## Installazione)
    - [Download ed Estrazione](### Download ed Estrazione)
    - [Set up delle variabili d'ambiente](### Setup delle variabili d'ambiente)
    - [Rimozione delle restrizione di MacOS](### Rimozione delle restrizione di MacOS)
    - [Compilazione](### Compilazione)
    - [Fix degli ultimi errori dell'IDE](### Fix degli ultimi errori dell'IDE)

## Requisiti
1. Installa XCode dall'App Store. Al primo avvio ti verrà chiesto di installare anche i command line tools. 
2. Installa Java 8 o versione successive. 
3. Installa Java 6 Legacy:
    1. Scarica il file da [questo link](https://support.apple.com/kb/dl1572?locale=en_US)
    2. Esegui questo AppleScript per renderlo eseguibile su Catalina:
        1. Apri l'applicazione Script Editor
        2. Copia ed esegui questo script:
            ```
            set theDMG to choose file with prompt "Please select javaforosx.dmg:" of type {"dmg"}
            do shell script "hdiutil mount " & quoted form of POSIX path of theDMG
            do shell script "pkgutil --expand /Volumes/Java\\ for\\ macOS\\ 2017-001/JavaForOSX.pkg ~/tmp"
            do shell script "hdiutil unmount /Volumes/Java\\ for\\ macOS\\ 2017-001/"
            do shell script "sed -i '' 's/return false/return true/g' ~/tmp/Distribution"
            do shell script "pkgutil --flatten ~/tmp ~/Desktop/Java.pkg"
            do shell script "rm -rf ~/tmp"
            display dialog "Modified Java.pkg saved on desktop" buttons {"Ok"}
            ```
        3. Nella finestra di selezione seleziona il file che hai scaricato in precedenza (il file .dmg)
        4. Va sul Desktop dove è comparso un file .pkg. Installalo. 

## Installazione di GDB
È consigliato (per semplicità) l'uso di `lldb` il debugger fornito da Apple. È comunque possibile installare GDB ma non funzionerà su Eclipse e andrà usato da linea di comando (come `lldb`). 


Se vuoi capire meglio cosa starai per fare guarda [questo link](https://www.thomasvitale.com/how-to-setup-gdb-and-eclipse-to-debug-c-files-on-macos-sierra/).


1. Usa [HomeBrew](https://brew.sh) per installare GDB `brew install gdb`
2. Apri l'applicazione "Accesso Portachiavi"
3. Dal menù in alto scegli Accesso Portachiavi -> Assistente Certificato -> Crea un certificato...
4. Chiamalo `gdb-cert` (il nome ci servirà più tardi)
5. Imposta:
    - **Tipo identità** a **Primo livello autofirmato**
    - **Tipo certificato** a **Firma codice**
    - Spunta la checkbox **Lascia che sia io a sovrascrivere i dati di default**
6. Vai "Next" per qualche volta finchè non leggi **Specifica una posizione per il certificato** come titolo, quindi imposta **Portachiavi** a **Sistema**.
7. Trova gdb-cert nella, doppio-cliccaci sopra (oppure tasto destro e "Ottieni informazioni) e apri la sezione **Attendibilità** e setta l'opzione **Firma codice** a **Fidati sempre**
8. Chiudi l'app "Accesso al portachiavi"
9. Riavvia il Mac
10. Crea il file `gdb_entitlement.xml` e copiace questo contenuto:
    ```xml
    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN"
    "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
    <plist version="1.0">
        <dict>
            <key>com.apple.security.cs.allow-jit</key>
                <true/>
            <key>com.apple.security.cs.allow-unsigned-executable-memory</key>
                <true/>
            <key>com.apple.security.cs.allow-dyld-environment-variables</key>
                <true/>
            <key>com.apple.security.cs.disable-library-validation</key>
                <true/>
            <key>com.apple.security.cs.disable-executable-page-protection</key>
                <true/>
            <key>com.apple.security.cs.debugger</key>
                <true/>
            <key>com.apple.security.get-task-allow</key>
                <true/>
        </dict>
    </plist>
    ```
10. Firma l'eseguibile: `sudo codesign --entitlements gdb-entitlement.xml -fs gdb-cert /usr/local/bin/gdb`

## Installazione 
Attenzione: per proseguire questa guida è necessario usare e sapere come funziona la shell, in particolare le variabili d'ambiente e i path. 

### Download ed Estrazione
1. Scarica Omnet++ dal [sito](https://omnetpp.org/download/)
2. Estrai l'archivio (puoi usare sia l'utility del Finder o il comando tar da terminale ``tar zxvf omnetpp-*.tgz``)

### Setup delle variabili d'ambiente
1. Aggiungi al file di configurazione delle tua shell (nel mio caso ~/.zshrc) le seguenti variabili d'ambiente:
    - `export PATH="$HOME/omnetpp-5.6.1/bin:$HOME/omnetpp-5.6.1/tools/macosx/bin:$PATH"`
    - `export QT_PLUGIN_PATH="$HOME/omnetpp-5.6.1/tools/macosx/plugins"`

**Attenzione: prima di fare questo assicurati che $HOME e la directory di Omnet siano corrette rispetto alla tua configurazione. Non fare solo copia e incolla**

2. Riavvia il terminal per applicare le modifiche. 

**N.B:** Aggiungere `tools/macosx/bin` al PATH potrebbe dare dei problemi con altri tools. Se qualche programma (a me lo ha dato con cmake) dovesse dare problemi puoi commentare la linea (`export PATH="$HOME/omnetpp-5.6.1/bin:$HOME/omnetpp-5.6.1/tools/macosx/bin:$PATH"`). 

### Rimozione delle restrizione di MacOS
1. Entra nella directory estratta
2. Esegui `sudo xattr -cr tools/macosx/bin`

Questo dovrebbe essere sufficiente a rendere eseguibili i tool necessari alla compilazione. In caso in fase di compilazione dovessero esserci ancora problemi con alcuni eseguibili disabilita spctl: `sudo spctl --master-disable`. Una volta che la compilazione è avvenuta con successo puoi riabilitarlo: `sudo spctl --master-enable`.

### Compilazione
1. Configura il makefile eseguendo `./configure`
2. Compila Omnet++ con `make`. Puoi velocizzare il processo con `make -j4` con 4 il numero di processori che vuoi usare. 

### Fix degli ultimi errori dell'IDE
#### Problemi al lancio dell'IDE
Prova a lanciare l'IDE:
- `omnetpp` da terminale
- Doppio click su omnetpp.app nella cartella ide

In caso di errori causati dall'assenza di alcuni file risolvere in questo modo:

1. Da terminale entrare dentro omnetpp.app (`cd ide/omnetpp.app`)
2. Identificare il path dei file "mancanti" (dovrebbero essere nella cartella `Contents/Eclipse/plugins`)
3. Modificare il file `Contents/Eclipse/omnetpp.ini` modificando i path di **-startup** e **--launcher.library**

#### Problemi al lancio delle simulazioni
In caso all'avvio di una simulazione dovesse comparire un errore relativo a qualche libreria cocoa mancante vuol dire che Eclipse non si è recuperato la variabile d'ambiente corretta. Per risolvere seguire questi passi:
1. Trova il bottone "Play" nella barra in alto e clicca sulla freccia che punta verso il basso alla destra del bottone. 
2. Scelgi "Run configurations"
3. Nel tab "Environment" aggiungi una nuova variabile (utilizza il bottone "Add" sulla destra) con nome `QT_PLUGIN_PATH` e valore `"$HOME/omnetpp-5.6.1/tools/macosx/plugins"` (**Controlla il path non fare semplicemente copia e incolla**).
