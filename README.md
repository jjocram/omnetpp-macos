# Omnet++ on MacOS Catalina
This guide is based on installation guide provided by Omnet++ (https://doc.omnetpp.org/omnetpp/InstallGuide.pdf) and adapted by me to work on MacOS Catalina.

## Readme language
- [English](https://gitlab.com/jjocram/omnetpp-macos/-/blob/master/eng_version.md)
- [Italian](https://gitlab.com/jjocram/omnetpp-macos/-/blob/master/ita_version.md)

## Index
- Requirements
- Install GDB (optional)
- Installation
    - Download and Extract
    - Set up the env variables
    - Remove MacOS restrictions
    - Compile
    - Fix last errors with the IDE