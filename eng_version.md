# Omnet++ on MacOS Catalina
This guide is based on installation guide provided by Omnet++ (https://doc.omnetpp.org/omnetpp/InstallGuide.pdf) and adapted by me to work on MacOS Catalina.

## Index
- [Requirements](##Requirements)
- [Install GDB](##Install gdb) (optional)
- [Installation](##Installation)
    - [Download and Extract](###Download and Extraction)
    - [Set up the env variables](###Setting up environmental variable)
    - [Remove MacOS restrictions](###Removing MacOS restriction for unsigned programs)
    - [Compile](###Compiling)
    - [Fix last errors with the IDE](###Fixing problems with the IDE)

## Requirements
1. Install XCode from the App Store and the command line tools (XCode request to install command line tools when it's lauched for the first time).
2. Install Java 8 or above.
3. Install Java 6 Legacy:
    1. Download the file from [here](https://support.apple.com/kb/dl1572?locale=en_US)
    2. Execute this AppleScript:
        1. Open Script Editor
        2. Copy and run this script:
            ```
            set theDMG to choose file with prompt "Please select javaforosx.dmg:" of type {"dmg"}
            do shell script "hdiutil mount " & quoted form of POSIX path of theDMG
            do shell script "pkgutil --expand /Volumes/Java\\ for\\ macOS\\ 2017-001/JavaForOSX.pkg ~/tmp"
            do shell script "hdiutil unmount /Volumes/Java\\ for\\ macOS\\ 2017-001/"
            do shell script "sed -i '' 's/return false/return true/g' ~/tmp/Distribution"
            do shell script "pkgutil --flatten ~/tmp ~/Desktop/Java.pkg"
            do shell script "rm -rf ~/tmp"
            display dialog "Modified Java.pkg saved on desktop" buttons {"Ok"}
            ```
        3. Select the file downloaded before (the .dmg file)
        4. Go to the Desktop and run the Java.pkg file

## Install gdb
You can also use `lldb` (recommended) that works out of the box in MacOs but if you want to use gdb you have to follow these steps; gdb will not work with Eclipse, you have to learn how to use it from the command line (neither lldb will work in Eclipse. It should work in XCode but It could be a bit difficult setting up the environment). 

Look at [this link](https://www.thomasvitale.com/how-to-setup-gdb-and-eclipse-to-debug-c-files-on-macos-sierra/) to understand a bit about what are you doing. 

1. Use [HomeBrew](https://brew.sh) to install gdb `brew install gdb`
2. Open Keychain Access application
3. From the menu choose eychain Access > Certificate Assistant > Create a Certificate...
4. Call it gdb-cert
5. Set:
    - **Identity Type** to **Self Signed Root**
    - **Certificate Type** to **Code Signing**
    - Select the **Let me override defaults**
6. Go "Next" for sometimes until you get to the **Specify a Location For The Certificate** screen, then set **Keychain** to **System**.
7. Find the gdb-cert in the list, double-click on it (or Get Info) and under Trust section set **Code Signing** to **Always Trust**
8. Close Keychan access app
9. Restart your Mac
10. Create the file `gdb_entitlement.xml` and paste this content:
    ```xml
    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN"
    "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
    <plist version="1.0">
        <dict>
            <key>com.apple.security.cs.allow-jit</key>
                <true/>
            <key>com.apple.security.cs.allow-unsigned-executable-memory</key>
                <true/>
            <key>com.apple.security.cs.allow-dyld-environment-variables</key>
                <true/>
            <key>com.apple.security.cs.disable-library-validation</key>
                <true/>
            <key>com.apple.security.cs.disable-executable-page-protection</key>
                <true/>
            <key>com.apple.security.cs.debugger</key>
                <true/>
            <key>com.apple.security.get-task-allow</key>
                <true/>
        </dict>
    </plist>
    ```
10. Sign the executable: `sudo codesign --entitlements gdb-entitlement.xml -fs gdb-cert /usr/local/bin/gdb`

## Installation
### Download and Extraction
1. Download Omnet++ files form the [website](https://omnetpp.org/download/)
2. Extract them (you can use both Finder and tar from terminal `tar zxvf omnetpp-*.tgz`)

### Setting up environmental variable
1. Add to your ".zshrc" file the following environment variables:
    - export PATH="$HOME/omnetpp-5.6.1/bin:$HOME/omnetpp-5.6.1/tools/macosx/bin:$PATH"
    - export QT_PLUGIN_PATH="$HOME/omnetpp-5.6.1/tools/macosx/plugins"

**BEFORE DOING IT CHECK YOUR DIRECTORIES POSITION: $HOME and omnet directory**

2. Restart your terminal to apply the new environmental variables

Adding `tools/macosx/bin` at the path could give problems with other tools. If some executable will broke this could be the cause. Comment the line which contains `export PATH=...`

### Removing MacOS restriction for unsigned programs
1. Enter in the directory extracted (omnetpp-VERSION)
2. Execute `sudo xattr -cr tools/macosx/bin`

If this isn't enough and you get some errors while compiling disable spctl: `sudo spctl --master-disable`. Remember to re-enable it when the process is complete: `sudo spctl --master-enable`

### Compiling
1. Configure the Makefile: `./configure`
2. Compile Omnet++: `make`. You can speed up the process with `make -j4` if you know what it means

### Fixing problems with the IDE
#### Launching the IDE
If you try to launch the IDE with `omnetpp` and get an errors for some missing files follow this steps:
1. Open the `ide` directory
2. Cd into the omnetpp.app: `cd omnetpp.app/Contents/Eclipse`
3. Open `omnetpp.ini` with a text editor and change the location of **-startup** and **--launcher.library** with the absolute path of te files 
Now you should be able to open the IDE with `omnetpp` or double click on the omnetpp.app icon (for some reason the command `omnetpp` from terminal is broken (it say the ide can't find some files. You have to launch from the icon (`ide/omnetpp.app` or search it using SpotLight)))
#### Launching simulations
When the IDE is launched you have to fix the "Run" configuration:
1. Find the "Play" button on the top bar and click on the arrow at its right ad choose "Run configurtions"
2. In "Environment" tab add a new variable ("Add" button on the right) with name `QT_PLUGIN_PATH` and value `"$HOME/omnetpp-5.6.1/tools/macosx/plugins"` (**Check the section [Setting up environmental variable](###Setting up environmental variable) to set the right value**)

### Everything is setted up
Now everythins should work and you can use Omnet++ on you Mac
